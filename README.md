# check-server-permissions



## Getting started

Execute
```
 ansible-playbook playbooks/provision.yml -i inventory/hosts --limit=dev
 ```
The final result is the server list and if the server is unreachable or not.
You can add more hosts to check in the inventory/hosts file
